#include "communication.h"

SOCKET communication::createUdpSocket(sockaddr_in Address)
{
	SOCKET Socket;

	//create socket
	Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (Socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	if (Socket < 0)
	{
		std::cerr << "Socket Initialization: Error creating socket" << std::endl;
		system("pause");
		WSACleanup();
		exit(11);
	}

	return Socket;
}


void communication::updateServer(std::queue<Input>* queue)
{
	char* buf;
	sockaddr_in addr = communication::getAddr("127.0.0.1", 6000);
	SOCKET Socket = communication::createUdpSocket(addr);
	INPUT inp = { 0 };
	bool b = false;
	
	while (true)
	{
		winWrap::inputMut->lock();
		b = queue->size() > 0;

		if (b)
		{
			inp = queue->front().data;
			//type = queue->front().type;
			queue->pop();
		}
		winWrap::inputMut->unlock();
		if (b)
		{
			buf = InputPacketSerializers::serializeMouseInput(inp);
			sendto(Socket, buf, 16, 0, (sockaddr*)&addr, sizeof(addr));
		}
	}
}


void communication::recieveFromServerM(std::queue<Input>* queue)
{
	char buf[16] = { 0 };
	SOCKET soc = communication::initiateUDPSocket(6002);
	INPUT inp = { 0 };

	while (true)
	{
		RECIEVE(soc, buf, 16);
		//std::cout << ((PMouseInput*)buf)->dx << " " << ((PMouseInput*)buf)->dy << ", " << ((PMouseInput*)buf)->mouseData << ", " << ((PMouseInput*)buf)->dwFlags << std::endl;
		inp.type = INPUT_MOUSE;
		inp.mi.dwExtraInfo = INJECTED_EI;
		inp.mi.dx = ((PMouseInput*)buf)->dx;
		inp.mi.dy = ((PMouseInput*)buf)->dy;
		inp.mi.mouseData = ((PMouseInput*)buf)->mouseData;
		inp.mi.dwFlags = ((PMouseInput*)buf)->dwFlags;
		//cout << "server sent data to client" << endl;
		winWrap::updateMut->lock();
		queue->push(Input{ MOUSE_IO, inp });
		winWrap::updateMut->unlock();
	}
}

void communication::recieveFromServerK(std::queue<Input>* queue, SOCKET soc)
{
	char* buf = new char[sizeof(PKeyboardInput)];

	//SOCKET soc = communication::connectToServer("127.0.0.1", SERVER_TCP_PORT);
	INPUT inp = { 0 };
	cout << "\nsocket is: " << soc << endl;
	while (true)
	{
		RECIEVE(soc, buf, sizeof(PKeyboardInput));
		PKeyboardInput iinput = *(PKeyboardInput*)(buf);
		inp.ki.wVk = iinput.wVk;
		inp.ki.wScan = iinput.wScan;
		inp.ki.dwFlags = iinput.dwFlags;

		winWrap::updateMut->lock();
		queue->push(Input{ MOUSE_IO, inp });
		winWrap::updateMut->unlock();
	}
}



SOCKET communication::connectToServer(std::string ip, int port)
{
	struct sockaddr_in sa = { 0 };
	SOCKET soc;

	soc = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (soc == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ "() --> socket creation");
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	//sa.sin_addr.s_addr = inet_addr((const char*)"127.0.0.1");
	sa.sin_addr.s_addr = inet_addr((const char*)ip.c_str());    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	cout << "\nConnecting to server on TCP port ...\n";
	while (true)
	{
		try
		{
			// again stepping out to the global namespace
			// Connects between the socket and the configuration (port and etc..)
			if (::connect(soc, (sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
				throw std::exception(__FUNCTION__ "() - connect");
			break;
		} catch (...) { }
	}

	// the main thread is only accepting clients 
	// and add then to the list of handlers
	cout << "Connected on TCP port" << endl;

	return soc;
}


void communication::updateServerKeyboard(std::queue<Input>* queue, SOCKET socket)
{
	char* buf;
	//sockaddr_in addr = communication::getAddr("127.0.0.1", 6000);
	//SOCKET Socket = communication::initTcpSocket(6000);
	INPUT inp = { 0 };
	const char len = 12;
	bool b = false;
	string log;

	while (true)
	{
		winWrap::inputKeyboardMut->lock();
		b = queue->size() > 0;

		if (b)
		{
			inp = queue->front().data;
			queue->pop();
		}
		winWrap::inputKeyboardMut->unlock();
		if (b)
		{
			buf = InputPacketSerializers::serializeKeyboardInput(inp);
			string str = "";
			PKeyboardInput iinput = *(PKeyboardInput*)(buf + sizeof(char) * 2);
			inp.ki.wVk = iinput.wVk;
			inp.ki.wScan = iinput.wScan;
			inp.ki.dwFlags = iinput.dwFlags;

			//SendInput(1, &inp, sizeof(INPUT));

			//cout << "The buffer:\n";
			/*for (int i = 0; i < 10; i++)
				str += std::to_string((int)*(buf + i)) + " ";*/

			//cout << endl << "The buffer:\n";
			log = "\n- sending data: ";
			log += "wVk - " + std::to_string(inp.ki.wVk) + ", ";
			log += "wScan - " + std::to_string(inp.ki.wScan) + ", ";
			log += "dwFlags - " + std::to_string(inp.ki.dwFlags) + "\n";
			cout <<  log.c_str();

			send(socket, buf, 10, 0);
			cout << "";
		}
	}
}


void communication::initWsa()
{
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
	{
		std::cerr << "Socket Initialization: Error with WSAStartup\n";
		system("pause");
		WSACleanup();
		exit(10);
	}
}


sockaddr_in communication::getAddr(string ip, int port)
{
	sockaddr_in Address;

	Address.sin_port = htons(port); // 6000
	Address.sin_family = AF_INET;
	Address.sin_addr.s_addr = inet_addr(ip.c_str());
	return Address;
}


SOCKET communication::initiateUDPSocket(int port)
{
	//init
	int server_length;
	SOCKET mySocket;
	sockaddr_in myAddress;

	//create socket
	mySocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (mySocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	if (mySocket < 0)
	{
		std::cerr << "Socket Initialization: Error creating socket" << std::endl;
		system("pause");
		WSACleanup();
		exit(11);
	}

	memset(&myAddress, 0, sizeof(myAddress));

	//bind
	myAddress.sin_family = AF_INET;
	myAddress.sin_addr.s_addr = INADDR_ANY;
	myAddress.sin_port = htons(port);

	if ((::bind(mySocket, (SOCKADDR*)&myAddress, sizeof(myAddress))) == SOCKET_ERROR)
	{
		std::cerr << "ServerSocket: Failed to connect\n";
		system("pause");
		WSACleanup();
		exit(14);
	}

	std::cout << "Listening for UDP on port " << port << std::endl;
	server_length = sizeof(struct sockaddr_in);

	return mySocket;
}


//SOCKET communication::initTcpSocket(int port)
//{
//	struct sockaddr_in sa = { 0 };
//	SOCKET soc;
//	soc = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//
//	if (soc == INVALID_SOCKET)
//		throw std::exception(__FUNCTION__ " - socket");
//
//
//	sa.sin_port = htons(port); // port that server will listen for
//	sa.sin_family = AF_INET;   // must be AF_INET
//	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"
//
//	// again stepping out to the global namespace
//	// Connects between the socket and the configuration (port and etc..)
//	if (::bind(soc, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
//		yeet std::exception(__FUNCTION__ " - bind");
//
//	return soc;
//}
