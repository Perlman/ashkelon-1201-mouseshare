#pragma once
#include "Resurces.h"

#define INJECTED_EI 0x69
#define VK_CAPS 20

#define KEYBOARD_IO 0
#define MOUSE_IO 1

typedef struct Input
{
	char type;
	INPUT data;
}Input;
 

typedef struct PKeyboardInput
{
	short    wVk;
	short    wScan;
	int   dwFlags;
}PKeyboardInput;


namespace winWrap
{

	extern std::queue<Input>* inputQueue;
	extern std::mutex* inputMut;
	extern std::mutex* updateMut;

	extern std::queue<Input>* inputKeyboardQueue;
	extern std::mutex* inputKeyboardMut;
	extern std::mutex* updateKeyboardMut;
	//extern std::queue<Input>* inputQueue;

	void setHook();
	LRESULT __declspec(dllexport)__stdcall  CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK MouseHookCallback(int nCode, WPARAM wParam, LPARAM lParam);
	void sendInp(std::queue<Input>* updateQ);
}