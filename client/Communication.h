#pragma once
#pragma comment (lib, "ws2_32.lib")
#include "Resurces.h"
#include "WINAPIWrapper.h"
#include "Serializers.h"

#define SERVER_TCP_PORT 6001

#define RECIEVE(soc, buf, size) \
if (recv(soc, buf, size, 0) < 0) \
{ \
	int err = WSAGetLastError();\
	if (err != 0)\
	{\
		closesocket(soc); \
	}\
	std::cerr << "Error recieving: " << err << " in "<< __FUNCTION__ << std::endl;\
	break; \
}

typedef struct PMouseInput
{
	short      dx;
	short      dy;
	int     mouseData;
	int     dwFlags;
}	PMouseInput;

namespace communication
{
	// Communicating with the server
	void updateServer(std::queue<Input>* queue);
	void recieveFromServerM(std::queue<Input>* queue);
	void recieveFromServerK(std::queue<Input>* queue, SOCKET soc);
	SOCKET connectToServer(std::string ip, int port);
	void updateServerKeyboard(std::queue<Input>* queue, SOCKET socket);

	// Creating things
	void initWsa();
	sockaddr_in getAddr(string ip, int port);
	SOCKET createUdpSocket(sockaddr_in Address);
	SOCKET initiateUDPSocket(int port);
}