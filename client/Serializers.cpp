#include "Serializers.h"


char* InputPacketSerializers::serializeMouseInput(INPUT inp)
{
	char buf[16] = { 0 };
	*(short*)buf = (short)inp.mi.dx;
	*(short*)(buf + sizeof(short)) = (short)inp.mi.dy;
	*(int*)(buf + sizeof(short) * 2) = (int)inp.mi.mouseData;
	*(int*)(buf + sizeof(short) * 2 + sizeof(int)) = (int)inp.mi.dwFlags;
	*(int*)(buf + sizeof(short) * 2 + sizeof(int) * 2) = (char)1;
	return buf;
}


//typedef struct tagKEYBDINPUT {
//	WORD    wVk;
//	WORD    wScan;
//	DWORD   dwFlags;
//	DWORD   time;
//	ULONG_PTR dwExtraInfo;
//} KEYBDINPUT, * PKEYBDINPUT, FAR* LPKEYBDINPUT;


char* InputPacketSerializers::serializeKeyboardInput(INPUT inp)
{
	//unsigned long
	//char buf[12] = { 0 };
	char* buf = new char[10];
	*(char*)(buf) = (char)KEYBOARD_IO;
	*(char*)(buf + sizeof(char)) = (char)8;
	*(unsigned short*)(buf + sizeof(char) * 2) = (unsigned short)inp.ki.wVk;
	*(unsigned short*)(buf + sizeof(char) * 2 + sizeof(unsigned short)) = (unsigned short)inp.ki.wScan;
	*(unsigned int*)(buf + sizeof(char) * 2 + sizeof(unsigned short) * 2) = (unsigned int)inp.ki.dwFlags;
 	return buf;
}