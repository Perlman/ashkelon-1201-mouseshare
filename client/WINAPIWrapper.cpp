#include "WINAPIWrapper.h"

#define SHIFT_VK 160
#define LOG false


#define print(out) \
if(LOG)\
	cout << out;\


using namespace std;
HHOOK _hook;


LRESULT __stdcall winWrap::MouseHookCallback(int nCode, WPARAM wParam, LPARAM lParam)
{
	POINT lastp;
	GetCursorPos(&lastp);
	MSLLHOOKSTRUCT* mouseInfo = (MSLLHOOKSTRUCT*)lParam;
	INPUT send;
	ZeroMemory(&send, sizeof(INPUT));

	send.type = INPUT_MOUSE;
	send.mi.dwExtraInfo = INJECTED_EI;
	send.mi.dwFlags = 0;
	send.mi.time = 0;
	send.mi.mouseData = 0;
	if (nCode >= 0 && mouseInfo->dwExtraInfo != INJECTED_EI)
	{
		try
		{
			switch (wParam)
			{
			case WM_LBUTTONDOWN:
				send.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
				break;
			case WM_LBUTTONUP:
				send.mi.dwFlags = MOUSEEVENTF_LEFTUP;
				break;
			case WM_RBUTTONDOWN:
				send.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
				break;
			case WM_RBUTTONUP:
				send.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
				break;
			case WM_MBUTTONDOWN:
				send.mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN;
				break;
			case WM_MBUTTONUP:
				send.mi.dwFlags = MOUSEEVENTF_MIDDLEUP;
				break;
			case WM_MOUSEMOVE:
				POINT p;
				p = mouseInfo->pt;

				send.mi.dx = p.x - lastp.x;
				send.mi.dy = p.y - lastp.y;
				send.mi.dwFlags = MOUSEEVENTF_MOVE;

				break;
			case WM_MOUSEWHEEL:
				send.mi.mouseData = static_cast<make_signed_t<WORD>>(HIWORD(mouseInfo->mouseData));
				//cout << "mouse wheel moved: " << static_cast<make_signed_t<WORD>>(HIWORD(mouseInfo->mouseData)) <<endl;
				send.mi.dwFlags = MOUSEEVENTF_WHEEL;
				break;
			}
			inputMut->lock();
			inputQueue->push(Input{ MOUSE_IO, send });
			inputMut->unlock();
		}
		catch (const std::exception& e)
		{
			cerr << e.what() << endl;
		}
		catch (...)
		{
			cerr << "uknown exception" << endl;
		}
	}
	else
	{
		//cout << "releasing data :)" << endl;
		return CallNextHookEx(_hook, nCode, wParam, lParam);
	}
	return 1;

	//return 0;
}



//typedef struct tagKEYBDINPUT {
//	WORD    wVk;
//	WORD    wScan;
//	DWORD   dwFlags;
//	DWORD   time;
//	ULONG_PTR dwExtraInfo;
//} KEYBDINPUT, * PKEYBDINPUT, FAR* LPKEYBDINPUT;


//ypedef struct tagKBDLLHOOKSTRUCT {
//	DWORD   vkCode;
//	DWORD   scanCode;
//	DWORD   flags;
//	DWORD   time;
//	ULONG_PTR dwExtraInfo;
//} KBDLLHOOKSTRUCT, FAR* LPKBDLLHOOKSTRUCT, * PKBDLLHOOKSTRUCT;

LRESULT __stdcall CALLBACK winWrap::KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	tagKBDLLHOOKSTRUCT* keyboardInfo = (tagKBDLLHOOKSTRUCT*)lParam;
	// 20 160
	bool caps = (GetKeyState(VK_CAPS) & 1) != 0;
	bool shift = GetKeyState(VK_SHIFT) < 0;
	bool upper = !(caps ^ shift);
	char key = ' ';

	INPUT send;
	ZeroMemory(&send, sizeof(INPUT));
	send.type = INPUT_KEYBOARD;
	send.ki.dwExtraInfo = INJECTED_EI;
	send.ki.wVk = keyboardInfo->vkCode;
	send.ki.dwFlags = 0;
	send.ki.time = 0;

	if (nCode >= 0 && keyboardInfo->dwExtraInfo != INJECTED_EI)
	{
		print("\nkeyboard input: ");
		print("ascii: " << keyboardInfo->vkCode << ", ");
		switch (wParam)
		{
			case WM_KEYDOWN:
			{
				print("status: down ");
				break;
			}
			case WM_KEYUP:
			{
				send.ki.dwFlags = KEYEVENTF_KEYUP;
				print("status: up ");
				break;
			}
			case WM_SYSKEYDOWN:
			{
				print("status: sys down ");
				break;
			}
			case WM_SYSKEYUP:
			{
				send.ki.dwFlags = KEYEVENTF_KEYUP;
				print("status: sys up ");
				break;
			}
		}

		if (keyboardInfo->vkCode >= 'A' && keyboardInfo->vkCode <= 'Z')
		{
			key = (char)(keyboardInfo->vkCode + upper * ('a' - 'A'));
			send.ki.wScan = key;
			print("info: " << key);
		}
		print(endl);

		inputMut->lock();
		inputKeyboardQueue->push(Input{ KEYBOARD_IO, send });
		inputMut->unlock();

		/*if (keyboardInfo->vkCode == VK_CAPS || keyboardInfo->vkCode == SHIFT_VK)
			return CallNextHookEx(_hook, nCode, wParam, lParam);*/
		//SendInput(1, &send, sizeof(INPUT));
		return 1;
	}
	/*else if (nCode >= 0)
	{
		return CallNextHookEx(_hook, nCode, wParam, lParam);
	}*/

	return  CallNextHookEx(_hook, nCode, wParam, lParam);
}


void winWrap::sendInp(queue<Input>* updateQ)
{
	INPUT toSend;

	bool b;
	while (true)
	{
		updateMut->lock();
		b = updateQ->size() > 0;
		if (b)
		{
			toSend = updateQ->front().data;
			updateQ->pop();
		}
		updateMut->unlock();
		/*if (b)
			cout << "injecting input, "  << toSend.ki.dwExtraInfo << endl;*/

		if (b)
		{
			toSend.ki.dwExtraInfo = INJECTED_EI;
			SendInput(1, &toSend, sizeof(INPUT));
			toSend.ki.wVk;
			toSend.ki.wScan;
			toSend.ki.dwFlags;
			cout << (string)((string)"injecting data: wVk - " + std::to_string(toSend.ki.wVk) + ", wScan - " + std::to_string(toSend.ki.wScan) + ", dwFlags - " + std::to_string(toSend.ki.dwFlags) + "\n").c_str();
		}
	}
}


void winWrap::setHook()
{
	//GetCursorPos(&lastp);
	// Set the hook and set it to use the callback function above
// WH_KEYBOARD_LL means it will set a low level keyboard hook. More information about it at MSDN.
// The last 2 parameters are NULL, 0 because the callback function is in the same thread and window as the
// function that sets and releases the hook. If you create a hack you will not need the callback function 
// in another place then your own code file anyway. Read more about it at MSDN.
	
	/*if (!(_hook = SetWindowsHookEx(WH_MOUSE_LL, winWrap::MouseHookCallback, NULL, 0)))
	{
		MessageBox(NULL, (LPCWSTR)"Failed to install mouse hook!", (LPCWSTR)"Error", MB_ICONERROR);
	}*/
	if (!(_hook = SetWindowsHookEx(WH_KEYBOARD_LL, winWrap::KeyboardProc, NULL, 0)))
	{
		MessageBox(NULL, (LPCWSTR)"Failed to install keyboard hook!", (LPCWSTR)"Error", MB_ICONERROR);
	}
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

}

