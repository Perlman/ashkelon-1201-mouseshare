#pragma once
#include "WINAPIWrapper.h"
#include "Serializers.h"
#include "communication.h"

//communication::createUdpSocket




using namespace std;

std::queue<Input>* winWrap::inputQueue = new std::queue<Input>;
std::mutex* winWrap::inputMut = new std::mutex;
std::mutex* winWrap::updateMut = new std::mutex;

std::queue<Input>* winWrap::inputKeyboardQueue = new std::queue<Input>;
std::mutex* winWrap::inputKeyboardMut = new std::mutex;
std::mutex* winWrap::updateKeyboardMut = new std::mutex;


void updateServerKeyboard(std::queue<Input>* queue, SOCKET socket)
{	
	char* buf;
	//sockaddr_in addr = communication::getAddr("127.0.0.1", 6000);
	//SOCKET Socket = communication::initTcpSocket(6000);
	INPUT inp = { 0 };
	bool b = false;	

	while (true)
	{
		winWrap::inputKeyboardMut->lock();
		b = queue->size() > 0;

		if (b)
		{
			inp = queue->front().data;
			queue->pop();
		}
		winWrap::inputKeyboardMut->unlock();
		if (b)
		{
			buf = InputPacketSerializers::serializeKeyboardInput(inp);
			send(socket, buf, 16, 0);
			//sendto(Socket, buf, 16, 0, (sockaddr*)&addr, sizeof(addr));
		}
	}
}


int main()
{
	SetProcessDPIAware();
	std::queue<Input>* updateMouseQueue = new std::queue<Input>;
	std::queue<Input>* updateKeyboardQueue = new std::queue<Input>;
	std::thread* t6;
	SOCKET serverTcpSoc = 0;
	communication::initWsa();

	// Initiating hooks
	std::thread t1(winWrap::setHook);
	
	// Initiating communication threads
	std::thread t2(communication::updateServer, winWrap::inputQueue);
	std::thread t3(communication::recieveFromServerM, updateMouseQueue);

	try
	{
		serverTcpSoc = communication::connectToServer("127.0.0.1", SERVER_TCP_PORT);
	}
	catch (const std::exception& e)
	{
		cout << "\nerror: " << e.what() << endl;
	}

	std::thread t4(communication::updateServerKeyboard, winWrap::inputKeyboardQueue, serverTcpSoc);
	std::thread t5(communication::recieveFromServerK, updateKeyboardQueue, serverTcpSoc);

	// Initiating input thraeds
	for (unsigned short i = 0; i < 2; i++)
	{
		t6 = new std::thread(winWrap::sendInp, updateMouseQueue);
		t6->detach();
		t6 = new std::thread(winWrap::sendInp, updateKeyboardQueue);
		t6->detach();
	}

	Sleep(4000);
	
	getchar();
	return 0;
}