#pragma once
#include "Buffer.hpp"
#include "Resurces.h"

#define KEYBOARD_INPUT 0
#define MOUSE_INPUT 1

typedef struct IInput
{
	int type;
} IInput;


typedef struct Lock
{
	mutex mutex_lock;
	unique_lock<mutex> lock;
} Lock;


typedef struct input
{
	Lock lock;
	std::queue<IInput*> input;
} input;


typedef struct Request
{
	int id;
	time_t recivalTime;
	buffer buffer;
} Request;



typedef struct PMouseInput
{
	short      dx;
	short      dy;
	int     mouseData;
	int     dwFlags;
}	PMouseInput;

typedef struct PKeyboardInput
{
	short    wVk;
	short    wScan;
	int   dwFlags;
}PKeyboardInput;

typedef struct MouseInput : IInput
{
	PMouseInput data;
}	MouseInput;

typedef struct KeyboardInput : IInput
{
	PKeyboardInput data;
} KeyboardInput;

namespace RequestPacketDeserializer
{
	unsigned int deserializeCode(buffer buffer);
	unsigned int deserializeLength(buffer buffer);
	IInput* deserializeInput(buffer buf);
	IInput* deserializeInputK(char* buf);
}