#pragma once

#pragma comment (lib, "ws2_32.lib")
//#include <Windows.h>
#include <queue>
#include "Resurces.h"
#include "Request.h"
#include <vector>
#include <exception>
#include "Client.h"
#include "InputStructures.h"
#include <map>

#define CODE_SIZE 1
#define MSG_LENGTH_SIZE 4
#define IO_PORT 6000
#define MAIN_REQUESTS_PORT 6001

#define RECIEVE(soc, buf, size) \
if (recv(soc, buf, size, 0) < 0) \
{ \
	/*cout << "problem :(" << endl;*/\
	int err = WSAGetLastError();\
	if (err != 0)\
	{\
		cout << "closing socket" << endl; \
		closesocket(soc); \
	}\
	cerr << "Error recieving: " << err << " in "<< __FUNCTION__ << endl;\
	yeet std::exception("");\
	/*break; */\
}
	
//std::map<int, int> sizes;


typedef struct Clients
{
	Lock lock;
	vector<Client> clients;
};

class Server
{
private:
	input _input;
	Clients _clients;
	SOCKET _serverSocket;
	Client* _activeClient;

	void initThreads();
	SOCKET initiateUDPSocket(int port);
	SOCKET initTCPSocket(int port);

	// Updating the main davice's data (nouse and keyboard input).
	// The functions for that are located in the Client class
	void getClientsIO();
	void updateActiveDevice();
	void controlPacketHandler();

public:
	Server();
	~Server();
	void bindAndListen(int port);
};