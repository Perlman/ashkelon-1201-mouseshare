#pragma once

#include "Server.h"
#include <thread>
#include <Windows.h>

std::map<char, char> sizes = {
	{ 0, 1 }
};


Server::Server()
{
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
	{
		cerr << "Socket Initialization: Error with WSAStartup\n";
		system("pause");
		WSACleanup();
		exit(10);
	}
	
	Client::udpSoc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (Client::udpSoc == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	if (Client::udpSoc < 0)
	{
		std::cerr << "Socket Initialization: Error creating socket" << std::endl;
		system("pause");
		WSACleanup();
		exit(11);
	}
	_activeClient = new Client(true);
	_input.lock.lock = unique_lock<mutex>(_input.lock.mutex_lock);
	_input.lock.lock.unlock();
	_clients.lock.lock = unique_lock<mutex>(_clients.lock.mutex_lock);
	_clients.lock.lock.unlock();
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_serverSocket);
	}
	catch (...) {}

	try
	{
		WSACleanup();
	}
	catch (...) {}
}

void Server::bindAndListen(int port)
{
	_serverSocket = initTCPSocket(port);
	sockaddr_in addr;
	int addrlen = sizeof(sockaddr_in);
	addr.sin_family = AF_INET;
	initThreads();
	Client* newClient;
	//Client* temp;

	while (true)
	{
		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(_serverSocket, (sockaddr*)&addr, &addrlen);

		cout << inet_ntoa(addr.sin_addr) << endl;
		if (client_socket == INVALID_SOCKET)
			yeet std::exception(__FUNCTION__);
		cout << "Client accepted. Server and client can speak" << endl;
		
		try
		{
			newClient = new Client(client_socket, addr, &_input);
			_clients.lock.mutex_lock.lock();
			_clients.clients.push_back(*newClient);
			_activeClient = newClient;
			_clients.lock.mutex_lock.unlock();
		}
		catch (const std::exception& e)
		{
			cerr << e.what() << " at function " << __FUNCTION__ << endl;
		}
	}
}

void Server::initThreads()
{
	
	thread* mainRequestThread = new thread(&Server::controlPacketHandler, &(*this));
	mainRequestThread->detach();
	thread* updateThread = new thread(&Server::updateActiveDevice, &(*this));
	updateThread->detach();
	thread* inputThread = new thread(&Server::getClientsIO, &(*this));
	inputThread->detach();
}

void Server::getClientsIO()
{
	const long maxUDPSize = 65535;
	unsigned int size = 0;
	buffer buf(maxUDPSize);
	SOCKET soc = initiateUDPSocket(IO_PORT);

	while (true)
	{	
		try
		{
			// Getting the code id
			RECIEVE(soc, buf.getbuffer(), buf.getSize());
			//cout << buf.getbuffer() << endl;

			// Here updating the input data
			_input.lock.mutex_lock.lock();
			_input.input.push(RequestPacketDeserializer::deserializeInput(buf));
			_input.lock.mutex_lock.unlock();
		}
		catch (const std::exception& e) 
		{
			cerr << e.what() <<" at function "<< __FUNCTION__ <<endl;
		}
	}
}

void Server::updateActiveDevice()
{
	while (true)
	{
		try
		{
			_input.lock.mutex_lock.lock();
			if (_input.input.size())
			{
				_activeClient->updateClient(_input.input.front());
				delete _input.input.front();
				_input.input.pop();
			}
			_input.lock.mutex_lock.unlock();
		}
		catch (const std::exception& e)
		{
			cerr << e.what() << " at function " << __FUNCTION__ << endl;
		}
	}
}

void Server::controlPacketHandler()
{
	// ****
	Request req;
	unsigned int size = 0;
	buffer length(MSG_LENGTH_SIZE);
	buffer id(CODE_SIZE);
	vector<Client> localClients;
	int code = 0;
	string log;
	int index = 0;

	char codeBuf = 0;
	char sizeBuf = 0;

	while (true)
{		_clients.lock.lock.lock();
		localClients = _clients.clients;
		_clients.lock.lock.unlock();
		index = 0;
		for (Client c : localClients)
		{
			if (!c.onServer() or true)
			{
				try
				{
					// Getting the code id
					RECIEVE(c.getSocket(), &codeBuf, CODE_SIZE);
					code = (int)codeBuf;//RequestPacketDeserializer::deserializeCode(id);
					//cout << "code is: " << code << ", ";
					/*log = (string)"code is: " + (char)(code + '0') + ", ";
					cout << log.c_str();*/

					// Getting data length
					RECIEVE(c.getSocket(), &sizeBuf, sizes.at(code));
					size = sizeBuf;//RequestPacketDeserializer::deserializeLength(length);
					/*log = (string)("size is: " + (char)(size + '0') + (char)", ");
					cout << log.c_str();*/

					// Getting the data
					char* buf = new char[size];
					RECIEVE(c.getSocket(), buf, size);
					RequestPacketDeserializer::deserializeInputK(buf);

					_input.lock.mutex_lock.lock();
					_input.input.push(RequestPacketDeserializer::deserializeInputK(buf));
					_input.lock.mutex_lock.unlock();
				}
				catch (const std::exception& e)
				{
					cout << e.what() << "\nError at " << __FUNCTION__ << endl;
					_clients.lock.lock.lock();
					_clients.clients.erase(_clients.clients.begin(), _clients.clients.begin() + index + 1);
					_clients.lock.lock.unlock();
				}
			}
			index++;
		}
		
	}
}

SOCKET Server::initiateUDPSocket(int port)
{
	//init
	int server_length;
	SOCKET mySocket;
	sockaddr_in myAddress;

	//create socket

	mySocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (mySocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	if (mySocket < 0)
	{
		cerr << "Socket Initialization: Error creating socket" << endl;
		system("pause");
		WSACleanup();
		exit(11);
	}

	memset(&myAddress, 0, sizeof(myAddress));

	//bind
	myAddress.sin_family = AF_INET;
	myAddress.sin_addr.s_addr = INADDR_ANY;
	myAddress.sin_port = htons(port);

	if ((::bind(mySocket, (SOCKADDR*)&myAddress, sizeof(myAddress))) == SOCKET_ERROR)
	{
		cerr << "ServerSocket: Failed to connect\n";
		system("pause");
		WSACleanup();
		exit(14);
	}

	cout << "Listening for UDP on port " << port << endl;
	server_length = sizeof(struct sockaddr_in);
	
	return mySocket;
}

SOCKET Server::initTCPSocket(int port)
{
	struct sockaddr_in sa = { 0 };
	SOCKET soc;
	soc = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (soc == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");


	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(soc, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		yeet std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(soc, SOMAXCONN) == SOCKET_ERROR)
		yeet std::exception(__FUNCTION__ " - listen");
	cout << "Listening for TCP on port " << port << endl;
	// the main thread is only accepting clients 
	// and add then to the list of handlers
	cout << "Waiting for client connection request" << endl;

	return soc;
}
