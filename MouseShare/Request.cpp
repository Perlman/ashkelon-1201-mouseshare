#include "Request.h"

unsigned int RequestPacketDeserializer::deserializeCode(buffer buffer)
{
	return *(int*)buffer.getbuffer();
}


unsigned int RequestPacketDeserializer::deserializeLength(buffer buffer)
{ 
	return *(int*)buffer.getbuffer();
}


IInput* RequestPacketDeserializer::deserializeInput(buffer buf)
{
	MouseInput* inp = new MouseInput;
	inp->data = *(PMouseInput*)buf.getbuffer();
	inp->type = MOUSE_INPUT;
	return inp;
}

IInput* RequestPacketDeserializer::deserializeInputK(char* buf)
{
	KeyboardInput* inp = new KeyboardInput;
	inp->data = *(PKeyboardInput*)buf;
	inp->type = KEYBOARD_INPUT;
	return inp;
}
