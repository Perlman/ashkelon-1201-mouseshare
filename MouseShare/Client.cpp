#include "Client.h"
#include "Server.h"

SOCKET Client::udpSoc = 0;
Client::Client(SOCKET soc, sockaddr_in from, input* input)
{
	from.sin_port = CLIENT_IO_PORT;
	_data.from = from;
	_id = 0;
	_onServer = false;
	_soc = soc;

	/*thread* clientKeyboardIO = new thread(&Client::listenToClientIO, &(*this), input);
	clientKeyboardIO->detach();*/
}

Client::Client(bool onServer)
{
	_data.from.sin_family = AF_INET;
	_data.from.sin_addr.s_addr = inet_addr("127.0.0.1");
	_data.from.sin_port = htons(6002);
	_onServer = onServer;
	_soc = NULL;
}

void Client::updateClient(IInput* input)
{
	KEYBOARD_INPUT;
	if (input->type == MOUSE_INPUT)
	{
		MouseInput* p = (MouseInput*)input;
		//cout << inet_ntoa(_data.from.sin_addr) << endl;
		sendto(udpSoc, (char*)&p->data, sizeof(PMouseInput), 0, (sockaddr*)&_data.from, sizeof(sockaddr_in));
		//cout << p->data.dx << " " << p->data.dy << ", " << p->data.mouseData << ", " << p->data.dwFlags << endl;
	}
	else
	{
		if (_soc == 0)
		{
			cout << "\nsocket is 0, can't sent keyboard input" << endl;
		}
		else
		{
			KeyboardInput* p = (KeyboardInput*)input;
			//cout << ("\nsocket is: " + (char)(_soc + '0') + (string)", " + (char)(this->getSocket() + '0') + "\n").c_str();
			
			/*PKeyboardInput iinput = *(PKeyboardInput*)((char*)&p->data);
			INPUT inp = { 0 };
			inp.ki.wVk = iinput.wVk;
			inp.ki.wScan = iinput.wScan;
			inp.ki.dwFlags = iinput.dwFlags;
			inp.ki.dwExtraInfo = 0x69;
			cout << "\nUpdating client... \n";

			SendInput(1, &inp, sizeof(INPUT));*/
			send(_soc, (char*)&p->data, sizeof(PKeyboardInput), 0);
		}
	}
}

void Client::listenToClientIO(input* input)
{
	const long maxUDPSize = 65535;
	//buffer buf(maxUDPSize);
	char* buf = new char[16];
	int type = 0;
	int size = 0;
	KeyboardInput* kInp = new KeyboardInput;
	INPUT send;
	
	//bad allocation
	//Error at Server::controlPacketHandler

	try
	{
		while (false)
		{
			RECIEVE(_soc, buf, 1);

			type = (int)buf[0];
			cout << "a message recieved" << endl;
				
			if (KEYBOARD_INPUT == type)
			{
				cout << "recieving for keyboard" << endl;

				RECIEVE(_soc, buf, 1);
				size = buf[0];

				RECIEVE(_soc, buf, 12);
				cout << "The buffer:\n";
				for (int i = 0; i < 12; i++)
					cout << (int)*(buf + i) + " ";
				cout << endl;
				/*try
				{
					RECIEVE(_soc, buf, 15);
				}
				catch (const std::exception& e)
				{
					cout << "\nerror: " << e.what() << endl;
				}*/

				kInp = (KeyboardInput*)RequestPacketDeserializer::deserializeInputK(buf + 1);
				send.ki.wVk = kInp->data.wVk;
				send.ki.wScan = kInp->data.wScan;
				send.ki.dwExtraInfo = kInp->data.dwFlags;

				SendInput(1, &send, sizeof(INPUT));
				input->lock.mutex_lock.lock();
				input->input.push(kInp);
				input->lock.mutex_lock.unlock();
			}
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}
}

SOCKET Client::getSocket()
{
	return _soc;
}

ClientData Client::getData()
{
	return _data;
}

bool Client::onServer()
{
	return _onServer;
}

int Client::getId()
{
	return _id;
}
