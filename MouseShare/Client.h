#pragma once
#include "Resurces.h"
//#include "Server.h"
#include "Request.h"
#include <Windows.h>

//#define KEYBOARD_INPUT 1
#define CLIENT_IO_PORT 6002


typedef struct ClientData
{
	sockaddr_in from;
} ClientData;

class Client
{
private:
	int _id;
	ClientData _data;
	SOCKET _soc;
	bool _onServer;

public:
	static SOCKET udpSoc;
	Client(SOCKET soc, sockaddr_in from, input* input);
	Client(bool);

	// This two functions need to update the mouse or the
	// keyboard of the client (in case he is the main device)
	void updateClient(IInput* input);
	void listenToClientIO(input* input);

	// Getters
	SOCKET getSocket();
	ClientData getData();
	bool onServer();
	int getId();
};
