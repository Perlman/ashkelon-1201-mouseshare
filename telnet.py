from socket import *
import struct
from threading import Thread
import time

DOTS = " ..."
PORT = 6001
INPUT_PORT = 6000


def send_udp(code, msg):
    sock = socket(AF_INET, SOCK_DGRAM)  # UDP
    sock.sendto(msg.encode(), ("127.0.0.1", INPUT_PORT))


def connect_to_server():
    print("Connecting socket" + DOTS)
    soc_address = ("127.0.0.1", PORT)
    soc = socket(AF_INET, SOCK_STREAM)

    while True:
        try:
            soc.connect(soc_address)
            print("Connected", "\n")
            return soc
            break
        except Exception:
            continue

def recieve_tcp(soc):
    return soc.recv(2048).decode()[4:]

def send_tcp(code, msg, soc):
    format_ = '=bi%ds' % len(msg)
    packed_data = struct.pack(format_, code, len(msg), msg.encode())

    try:
        soc.sendall(packed_data)
    except Exception as e:
        print("Exception: " + e)
        exit()

def talk_with_server(code, msg, soc, addition="", to_print=True):
    format_ = '=bi%ds' % len(msg)
    packed_data = struct.pack(format_, code, len(msg), msg)
    ans = ""

    # Connecting to port and talking with server
    try:
        soc.sendall(packed_data)
        if code is not 3 or 0:
            ans = soc.recv(2048)
            if to_print:
                print(addition + "response is: " )
                print(ans[4:].decode())
    except Exception as e:
        print("Exception:")
        print(e)
        exit()
    if to_print:
            print()
    return ans[4:].decode()

def spam_udp():
    while True:
        send_udp(2, "fucking awesome\x00")

def main():
    soc = connect_to_server()
    t = Thread(target=spam_udp)
    t.start()
    while True:
#        send_udp(2, "fucking awesome\x00")
        send_tcp(0x0,"hello",soc)
        print(recieve_tcp(soc))
        time.sleep(1)
        
    # connect_to_server(USER4)
    # signup(USER4)


if __name__ == "__main__":
    main()


